# Geographical Information System Libraries

This set of libraries is crafted to offer powerful and adaptable tools for managing geographical data.

## Integration with LidarView or ParaView

Explore the integration of these libraries with LidarView through a dedicated plugin available [here](https://gitlab.kitware.com/LidarView/plugins/gis-tools).

## License

This code is distributed under the Apache 2.0 license.
